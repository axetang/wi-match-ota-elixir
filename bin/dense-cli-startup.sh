#!/bin/bash

ORCHNAME="orch"

EMUBOOT=/var/emulab/boot
REPODIR=/local/repository
SHOUTSRC=/local/repository/shout
CLILOG=/var/tmp/ccontroller.log

echo "Preparing to start Shout client.  Please wait."

cd $REPODIR
git submodule update --init --remote || \
    { echo "Failed to update git submodules!" && exit 1; }

sudo ntpdate -u ops.emulab.net && \
    sudo ntpdate -u ops.emulab.net || \
	echo "Failed to update clock via NTP!"

sudo rm -f $CLILOG

ORCHDOM=`$REPODIR/bin/getexpdom.py`
ORCHHOST="$ORCHNAME.$ORCHDOM"

echo "Starting Shout client on `hostname -s`"

$SHOUTSRC/meascli.py -t -s $ORCHHOST

exit 0
