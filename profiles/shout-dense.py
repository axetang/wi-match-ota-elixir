#!/usr/bin/python

"""Use this profile to run ***Shout*** continuous wave (CW) power measurements
across two or more radios at POWDER's Dense Deployment sites.

More information on *Shout* (and the associated *WiMatch* solver) can
be found in [this paper](https://www.flux.utah.edu/download?uid=303)
and the source code is available
[here](https://gitlab.flux.utah.edu/powderrenewpublic/shout).

There are six Dense Deployment sites, which can be seen as green icons
on this page:

  [POWDER Deployment Map](https://powderwireless.net/map)

As of June 2022, Each Dense site has a single ruggedized NI B210
connected to a small form factor "NUC-like" compute node.  Please ask
on the POWDER Users Google Group about possible updates to the
equipment present.  It is expected that the site equipment will change
over time.  Another good resource to check is the radio information
page (must be logged in to the POWDER web UI):

  [POWDER Radio Info](https://www.powderwireless.net/radioinfo.php)

**Instantiate this profile with appropriate parameters**

At the "Parameterize" step, you will want to add as many "Dense
Deployment" radios as are available.  Click on the "Resource
Availability" button near the top of the page to check availability.
Click the "+" symbol to add to the list of radios to allocate; you'll
need a line for each individual site you want to include.  You need
at least two sites to perform measurements as the radio clients will
take turns acting as the transmitter while the rest receive.

You will also need to add one (or more) CBRS/CBAND frequency range(s).
Check the bottom of the "Resource availability" page for frequency
ranges that are currently in use, and chose CBRS/CBAND frequencies
that are not listed.  A single default range is listed on the
parameterize step by default, but may be in use (check first).

You can leave the other parameters in this profile at their defaults.

Once you have these parameters selected, click through the rest of the
profile and then click "Finish" to instantiate.  It will take 10 to 15
minutes for the experiment to finish setting up.  Once it is "green",
proceed to this profile's instructions for information on performing
measurments.

Instructions:

Follow these steps to execute one or more measurement runs with the
*Shout* measurement tool on the POWDER Dense Deployment sites in your
experiment:

**1) Open SSH sessions**

*Note:* POWDER requires SSH key-based authentication.  You can
upload a key and learn more about using/generating keys by clicking
"Manage SSH Keys" from the top right drop-down menu showing your
username.

As you may be opening quite a number of SSH sessions here, you may
want to look into a multi-terminal session manager like `tmux`,
`Screen`, or another GUI terminal app that supports multiple sessions.
You will also want to have an X session running on your local machine
that you can forward through your SSH sessions.

Open an SSH session to each of the `cnode-<site>-b210` nodes, and two
sessions on the `orch` node.  You should forward your X session to one
or both of the `orch` ssh sessions, e.g., with `ssh -Y
<username>@<orch_node_hostname>`.

Reference SSH commands can be seen on the "List View" tab next to the
compute node names.  If you have an `ssh://` handler setup on your
browser, you can click these commands to open a corresponding SSH
session (they are hyperlinks).

**2) Start the Shout Orchestrator process**

In one of your `orch` SSH sessions, run:

```
/local/repository/bin/orch-startup.sh
```

This will start the Shout orchestrator that all of the measurement
clients, and the command executor program will connect to.

**3) Start measurement clients**

In the SSH session for each of the `cnode-<site>-b210` nodes, run:

```
/local/repository/bin/dense-cli-startup.sh
```

You should see these clients connect in the output of the client, and
in the output of the orchestrator.

**4) Execute a measurement run**

*Note:* Before executing the rest of the commands below on the
`orch` host, first change directory to the 'Shout' source repository's
location:

```
cd /local/repository/shout
```

With all clients connected to the orchestrator, you can now perform a
measurement collection run.  There is a JSON command file located
here: `/local/repository/etc/dense-run.json`.  If you specified
anything other than 3400 - 3410 MHz when instantiating your
experiment, you will need to change the frequency in this file to
match the bottom of the downlink range you specified.  E.g., if you
specified 3450 - 3460 MHz, then change the frequency to "3450" in this
command file.  The command file is setup to capture measurements every
500 KHz across 10 MHz of bandwidth starting at the afforementioned
starting frequency.  Once the command file is properly adjusted,
execute the following command in your other `orch` SSH session:

```
./measiface.py -c /local/repository/etc/dense-run.json
```

This will kick off the collection run, which should take perhaps 10
minutes to complete.  The process will iterate through each radio
device in turn, having it act as transmitter.  The rest of the radios
will act as receivers.  First, "noise floor" measurements for each
receiver will be gathered, and then another round of measurments with
the transmitter running will be done.  You can see the frequency steps
(500 KHz by default) occuring within each round in the output of the
measurment clients.

You can watch the SSH session windows to see the measurements
progress.  There will be a `measurements.hdf5` file in a new
`mcondata` subdirectory (full path:
`/local/repository/shout/mcondata/measurements.hdf5`) once you start
running measurements.  This file will contain all of the measurements
taken by the Shout runs you perform during this experiment.

**5) Check for collected data and produce graphs**

*Note:* Run your SSH session for these commands such that it forwards
your local window manager session, e.g.: `ssh -Y username@host`

Run `./analyze-data.py -l` to confirm that you have measurement data
entries.  You should see output lines that look like this:

```
/measure_paths/1656614736/cnode-mario-dd-b210/cnode-wasatch-dd-b210
```

The initial `measure_paths` part indicates path measurements data.
The following numeric field is a UNIX timestamp representing when the
measurement run started; if you perform multiple measurement runs,
each run will appear under a different/unique timstamp. The next field
is the transmitter device, followed by the receiver device. These
output lines indicate measurements were taken for each of the pairs
listed.

Next, run:

```
./analyze-data.py -m -s -d
```

This will produce graphs of continuous wave power above noise plus
interference versus distance, and power across the 500KHz (default)
steps for each TX/RX pair (in both directions) from the samples
collected during the above measurement run.

You should not expect "full mesh" results.  Values at or below "10 dB"
in the pairwise measurements graph often indicate measurements
of the noise floor. Adjacent sites will show higher signal strength
results.

A second graph plotting power over noise vs. distance, and a linear regression
of these measurments, will also be shown.

**Further exploration**

You can perform additional data collection runs (step 4 above),
changing sampling rates, gains, step size, etc. as desired.  The
`analyze-data.py` script has parameters for selecting specific
timestamps or time ranges for graphing data, among other options. You
can explore the power spectral density plots for individual
transmit/receive pairs using the `-p` option.  See the output of
`analyze-data.py -h` for more information.

*NOTE:* Be certain that you do not specify transmission in spectrum
outside of what you have declared for this experiment!  The sampling
rate in the JSON file is twice the bandwidth that will be "walked" by
the continuous wave transmissions.  This sampling rate should be no
more than 2 times the bandwidth you have declared for the experiment.

You can copy the '/local/repository/shout/mcondata/measurements.hdf5'
file using `scp` or a similar tool to your own machine if you wish to
save off the data you have collected.

"""

# Library imports
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.emulab.spectrum as spectrum
import geni.rspec.igext as ig

# Global Variables
meas_disk_image = \
        "urn:publicid:IDN+emulab.net+image+PowderTeam:U18-GR-PBUF"
orch_image = meas_disk_image
x310_node_image = meas_disk_image
nuc_image = meas_disk_image
sm_image = meas_disk_image
clisetup_cmd = "/local/repository/bin/cli-startup.sh"
orchsetup_cmd = "/local/repository/bin/orch-startup.sh"
MIN_CBAND = 3350
MAX_CBAND = 3600

# Top-level request object.
request = portal.context.makeRequestRSpec()

# List of CBRS rooftop X310 radios.
dense_radios = [
    ("cnode-wasatch",
     "Wasatch"),
    ("cnode-mario",
     "Mario"),
    ("cnode-moran",
     "Moran"),
    ("cnode-guesthouse",
     "Guesthouse"),
    ("cnode-ebc",
     "EBC"),
    ("cnode-ustar",
     "USTAR"),
]

# Set of OTA Lab NUC+B210 devices to allocate
portal.context.defineStructParameter(
    "dense_radios", "Dense Site Radios", [],
    multiValue=True,
    min=2,
    multiValueTitle="Dense Site NUC+B210 radios to allocate.",
    members=[
        portal.Parameter(
            "device",
            "SFF Compute + NI B210 device",
            portal.ParameterType.STRING,
            dense_radios[0], dense_radios,
            longDescription="A Small Form Factor compute with attached NI B210 device at the given Dense Deployment site will be allocated."
        ),
    ])

# Frequency/spectrum parameters
portal.context.defineStructParameter(
    "cband_freq_ranges", "CBRS / CBAND Frequency Ranges", [],
    multiValue=True,
    min=1,
    multiValueTitle="Frequency ranges for CBRS / CBAND operation.",
    members=[
        portal.Parameter(
            "freq_min",
            "Frequency Min",
            portal.ParameterType.BANDWIDTH,
            3400.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
        portal.Parameter(
            "freq_max",
            "Frequency Max",
            portal.ParameterType.BANDWIDTH,
            3410.0,
            longDescription="Values are rounded to the nearest kilohertz."
        ),
    ])

# Node type for the orchestrator.
portal.context.defineParameter(
    "orchtype",
    "Orchestrator node type",
    portal.ParameterType.STRING, "d430",
    ["d430","d740"],
    "Type of compute node for the orchestrator.",
    advanced=True
)

# Bind and verify parameters
params = portal.context.bindParameters()

for i, frange in enumerate(params.cband_freq_ranges):
    if frange.freq_min < MIN_CBAND or frange.freq_min > MAX_CBAND \
       or frange.freq_max < MIN_CBAND or frange.freq_max > MAX_CBAND:
        perr = portal.ParameterError("CBAND frequencies must be between %d and %d MHz" % (MIN_CBAND, MAX_CBAND), ["cband_freq_ranges[%d].freq_min" % i, "cband_freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)
    if frange.freq_max - frange.freq_min < 1:
        perr = portal.ParameterError("Minimum and maximum frequencies must be separated by at least 1 MHz", ["cband_freq_ranges[%d].freq_min" % i, "cband_freq_ranges[%d].freq_max" % i])
        portal.context.reportError(perr)

# Now verify the parameters.
portal.context.verifyParameters()
        
# Allocate orchestrator node
orch = request.RawPC("orch")
orch.disk_image = orch_image
orch.hardware_type = params.orchtype
#orch.addService(rspec.Execute(shell="bash", command=orchsetup_cmd))

# Request NUC+B210 radio resources at the requested Dense Deployment sites.
for dev in params.dense_radios:
    node = request.RawPC("%s-dd-b210" % dev.device)
    node.component_id = dev.device
    node.disk_image = sm_image
    #node.addService(rspec.Execute(shell="bash",
    #                              command=clisetup_cmd + " %s" % orch.name))

# Request frequency range(s)
for frange in params.cband_freq_ranges:
    request.requestSpectrum(frange.freq_min, frange.freq_max, 0)

# Emit!
portal.context.printRequestRSpec()
